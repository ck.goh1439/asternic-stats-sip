# asternic-stats-sip

Asternic Call Center Stats will let you run reports over your Asterisk PBX queue activity, 
eg. how many calls were abandoned, how many answered, by whom, call durations, wait times, 
call distribution per different durations.